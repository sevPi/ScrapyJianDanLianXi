# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class ScrapytestItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()

    name=scrapy.Field() #作品名称
    category=scrapy.Field() #作品类别
    novel_url = scrapy.Field()  # 小说链接
    writestatus=scrapy.Field() #作品更新状态
    author=scrapy.Field() #作者名称
    #更新时间

