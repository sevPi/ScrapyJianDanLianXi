from .sql import Sql
from scrapyTest.items import ScrapytestItem

class ScrapyTestPipeline(object):

    def process_item(self,item,spider):
        if isinstance(item,ScrapytestItem):
            name=item['name']
            res=Sql.select_name(name)
            if res[0]==1:
                print('已存在')
            else:
                xs_name=item['name']
                xs_category=item['category']
                xs_url=item['novel_url']
                Sql.insert_xs_info(xs_name,xs_url,xs_category)
                print("开始存储")