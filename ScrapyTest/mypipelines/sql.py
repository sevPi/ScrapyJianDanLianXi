import mysql.connector
from scrapyTest import settings


MYSQL_HOSTS='127.0.0.1'
MYSQL_USER='root'
MYSQL_PASSWORD='5678'
MYSQL_PORT='3306'
MYSQL_DB='test'


cnx=mysql.connector.connect(user=MYSQL_USER,password=MYSQL_PASSWORD,host=MYSQL_HOSTS,database=MYSQL_DB)
cur=cnx.cursor(buffered=True)

class Sql:
    @classmethod
    def insert_xs_info(cls,xs_name,xs_url,xs_category):
        sql='INSERT INTO novel_list(`xs_name`,`xs_url`,`xs_category`)VALUES (%(xs_name)s,%(xs_url)s,%(xs_category)s)'
        value={
            'xs_name':xs_name,
            'xs_category':xs_category,
            'xs_url':xs_url
        }
        cur.execute(sql,value)
        cnx.commit()
    @classmethod
    def select_name(cls,xs_name):
        sql='SELECT EXISTS(SELECT 1 FROM novel_list WHERE xs_name=%(xs_name)s)'
        value={
            'xs_name':xs_name
        }
        cur.execute(sql,value)
        return cur.fetchall()[0]

