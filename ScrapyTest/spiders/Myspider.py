import scrapy
from bs4 import BeautifulSoup
from scrapy.http import Request
from scrapyTest.items import ScrapytestItem


class Myspider(scrapy.Spider):
    name = 'scrapyTest'
    allow_domains=["top.17k.com"]
    start_urls=["http://top.17k.com/top/top100/06_vipclick/06_vipclick_serialWithLong_top_100_pc.html",
             "http://top.17k.com/top/top100/01_subscribe/01_subscribe__top_100_pc.html",
             "http://top.17k.com/top/top100/06_vipclick/06_vipclick_cnl_man_top_100_pc.html",
             "http://top.17k.com/top/top100/06_vipclick/06_vipclick_cnl_mm_top_100_pc.html"
             ]
    def start_requests(self):
        for i in range(0,len(self.start_urls)):
            url=self.start_urls[i]
            yield Request(url,self.get_novelname)


    def get_novelname(self,response):
        tds=BeautifulSoup(response.text,'lxml').find('div',class_='BOX').find('table')
        item = ScrapytestItem()
        for td in tds.children:
            try:
                novel_name=td.find('a',class_='red').get_text()
                novel_url=td.find('a',class_='red')['href']
                novel_category=td.find('a').get_text()
                item['name']=novel_name
                item['novel_url']=novel_url
                item['category']=novel_category
                yield item
            except:
                continue




